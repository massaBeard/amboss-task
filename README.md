# Ensure these are done first!

Install java SDK from here: https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html

Please be sure to select the one for Windows x64.

Install Eclipse IDE 2019-06 here: https://www.eclipse.org/downloads/


## Syncing Git Repository with Eclipse

Copy your Git Project HTTPS URL.

This is the url provided here: https://gitlab.com/massaBeard/amboss-task.git

In Eclipse, make sure that you have the "Git Repositories" view available
Go to Window --> Show View --> Other --> Click on Git --> Git Repositories

Now in the Git window, click on Clone a Git repository.

Your copied Git repository will auto-fill.

Type in the user name and password. (UN: **massaBeard** PW: **&?3N59x2KfhU5sM** )
Choose "Store in Secure Store".

Click Next, and then Next again.

At this window, you will see Finish, but before you click, use correct destination directory.

Choose your "workspace" directory that you created when you installed eclipse.

Click Finish, and wait a few minutes for Eclipse to bring-in the Gitlab project.

Your project will show under "Git Repositories" section.

**PLEASE ENSURE THAT ALL FILEPATHS ARE THE CORRECT ONES FOR YOUR LIBRARIES AND FOLDERS, THE PROJECT WON'T RUN WITHOUT IT**


### Running the tests I created



To run the tests, go into the amboss-task folder --> src folder --> unitTests --> Choose a test.

Now when you double click to open the tests you can then click on either the run as button, which should automatically run the test you're currently in OR you can go to the Run dropdown above (with the test still highlight in the view) and click run.

Currently, due to time constraints and lack of paid tech needed (how I learned to do it) the tests are only running on Chrome browser locally. Also, they are very basic functionally and would need more time to be fully fleshed out, but I believe, accomplish the tasks required on the basic level.


** All libraries used were downloaded via Selenium website (jar files and driver files) and Automatically retrieved by Maven and TestNG when they were installed **