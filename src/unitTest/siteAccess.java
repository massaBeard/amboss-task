package unitTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


@SuppressWarnings("unused")
public class siteAccess {

	private static WebDriver driver;
	static String URL = "https://qa.amboss.us.qa.medicuja.de/account/login";
	
	@BeforeClass
	public void testSetUp() {

		driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public static void accessAmboss() throws InterruptedException {

		System.out.println("Navigating to Amboss");

		driver.get(URL);
		Thread.sleep(2000);
	}

	@AfterClass
	public void tearDown() {
			
			driver.quit();
		}

}
