package unitTest;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@SuppressWarnings("unused")

public class siteLogin {

	private static WebDriver driver;
	static String URL = "https://qa.amboss.us.qa.medicuja.de/account/login";

	@BeforeClass
	public void testSetUp() {

		driver = new ChromeDriver();
	}

	@Test(priority = 1)
	public static void accessAmboss() throws InterruptedException {

		System.out.println("Navigating to Amboss");

		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
		SwitchTOOriginalWindow();

		driver.get(URL);
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@id='signin_username']")).sendKeys("");
		Thread.sleep(1000);

		driver.findElement(By.xpath("//input[@id='signin_password']")).sendKeys("");
		Thread.sleep(1000);

		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		Thread.sleep(3000);

		SwitchTOWindow();

		driver.get("https://qa-assignment.us.next.medicuja.de/us/");

		driver.findElement(By.xpath("//*[@id='details-button']")).click();
		driver.findElement(By.xpath("//*[@id='proceed-link']")).click();
		Thread.sleep(5000);
	}

	public static void SwitchTOOriginalWindow() throws InterruptedException {

		String parentWindowHandler = driver.getWindowHandle();// Store your parent window
		String subWindowHandler = null;
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(parentWindowHandler); // switch to popup window
	}

	public static void SwitchTOWindow() throws InterruptedException {

		String parentWindowHandler = driver.getWindowHandle();// Store your parent window
		String subWindowHandler = null;
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.close();
		driver.switchTo().window(subWindowHandler); // switch to popup window
	}

	public void openNewTab() {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
	}
	
	@AfterClass
	public void tearDown() {
			
			driver.quit();
		}

}
